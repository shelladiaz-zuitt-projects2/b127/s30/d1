const express = require('express');
//mongoose is a package that allows creation of schemas to model our data structures
//also has access to a number of methods form manipulating our datababse
const mongoose = require('mongoose');
const app = express();
const port = 3001;

//mongodb atlas
//connecting monggodb
//connect to the database by pasing in your connecting string, remeber to replaace <password> and database name with actual values
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.dxzdo.mongodb.net/batch127_to-do?retryWrites=true&w=majority", 
    {
        useNewUrlParser: true, //avoid any future errors when connecting mongodb
        useUnifiedTopology: true //avoid any future errors when connecting mongodb
    }
);
//connecting to robo3T locally
/**
 * mongoose.connect("mongodb://localhost:27017/database name", 
    {
        useNewUrlParser: true, //avoid any future errors when connecting mongodb
        useUnifiedTopology: true //avoid any future errors when connecting mongodb
    }
);
 */

//set notifications for connection success or failure
//connection to the database, allows us to handle errors when the initial connection is established
let db = mongoose.connection;
//if a connection error occured, outuput in the console
//console.error bind(console) = allows us to print error in the browser console and in the terminal

db.on("error", console.error.bind(console, "connection error"));
//if the connection is successful, output in the console
db.once("open", ()=> console.log("We're connected to the cloud database"))


app.use(express.json());
app.use(express.urlencoded({ extended:true }));

//Mongoose Schemas
//Schemas determine the structure of the documents to be written in the database
//schemas act as blueprints to our data
//use the schema() constructor of the Mongoose module to create a new Schema object
//the "new" keyword create a new schema
const taskSchema = new mongoose.Schema({
    //define the fields with the corresponding data type
    //for task, it needs a "task name" and "status"
    name: String,
    status: {
        type: String,
        //default values are the pre defined values for a filed if we dont put any value
        default: "pending"
    }
})
//Task is capitalized following the MVC approach for naming convention
//Model-View-Controller
//first parameter of the Mongoose model method indicates the collection in where to store the data
//the second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
const Task = mongoose.model("Task", taskSchema)


//create anew task
/***
 * Business Logic
 * 1. Add a funtionality to check if there are duplicate tasks
 * -if the task already exists in the database, we return an error
 * -if the task doesnt exist in the database, we add it in the database
 * 2. The task data will be coming from the request's body
 * 3. Create a new Task object with a "name" field/property
 * 4. The "status" property does not need to be provided beacuse our schema default is to "pending" upon creation of an object
 */
app.post('/tasks', (request, response)=> {
    //check if there are duplication tasks
    //findOne is a mongoose mwthod that acts similar to "find" of MongoDB
    //findOne() returns the first document that mathces the search criteria
    Task.findOne({ name: request.body.name }, (err, result)=> {
        //if a document was found and the document's name mathces the information set via the client/postman

        if(result !== null && result.name == request.body.name){
            //return a message to the client
            return response.send("Duplicate task found")
        }else{
            //if no document was found, create a new task and save it to our database
            let newTask = new Task({
                name: request.body.name
            })
            //.save store new information
            newTask.save((saveErr, savedTask)=>{
                if(saveErr){
                    return console.error(saveErr)
                }else{
                    return response.status(201).send("New task created")
                }
            })
        }
    })
})

//Getting all the tasks
/***
 * Business Logic
 * 1. we will retrieve all the documents using the get method
 * 2. if an error is encountered, print the error
 * 3. if no errors are found, send a success status back to the client/postman and return an array of documents
 * 
 */
app.get('/tasks', (request, response)=> {
    //"find" is a mongoose method that is similar to a MongoDB "find"
    //empty {} means it return all the documents and stores them in the result parameter of the callback function
    Task.find({}, (err, result)=>{
        //if an error occured
        if(err){
            return console.log(err)
        }else{
            //if no errors are found
            //status 200 means that everythin is OK in terms of processing
            //"json" method allows to send a JSON format for the response
            //The returned reponse is purposefully returned as an object with the "data" property to mirror real worl complex data structures
            return response.status(200).json({
                data: result
            })
        }
    })
})

/**
 * Activity:
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a GET route that will return all users.
6. Process a GET request at the "/users" route using postman.
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of s30 Activity.
9. Add the link in Boodle named Express js Data Persistence via Mongoose ODM.
*
*Business Logic for POST /signup
1. Add a functionality to check if there are duplicate tasks
-If the user already exists in the database, we return an error
-If the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Create a new user object with a "username" and "password" fields/properties
Business Logic for GET /users
1. We will retrive all the documents using the get method
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/postman and return an array of document
 */

const userSchema = new mongoose.Schema({
   
    username: String,
    password: String

})
const User = mongoose.model("User", userSchema)


app.post('/signup', (request, response)=> {
    User.findOne({ username: request.body.username, password: request.body.password}, (err, result)=> {
        if(result !== null && result.username == request.body.username){
            return response.send("Duplicate user found")
        }else{
            let newUser = new User({
                username: request.body.username,
                password: request.body.password
            })
            newUser.save((saveErr, savedUser)=>{
                if(saveErr){
                    return console.error(saveErr)
                }else{
                    return response.status(201).send("New user created")
                }
            })
        }
    })
})

app.get('/users', (request, response)=> {
  
    User.find({}, (err, result)=>{
        if(err){
            return console.log(err)
        }else{
           
            return response.status(200).json({
                data: result
            })
        }
    })
})


app.listen(port, () => console.log(`server is running at port ${port}`));